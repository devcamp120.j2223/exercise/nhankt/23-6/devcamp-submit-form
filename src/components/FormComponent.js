import { Component } from "react";
class FormComponent extends Component {
    onInputChange = (event) =>{
        console.log(event.target.value)
    }
    addTaskHandler = (event) => {
        event.preventDefault();
        console.log("da submit form")
    }
    render() {
        return (
            <div style={{ backgroundColor: "#C6DCE4"}}>
                <form onSubmit={this.addTaskHandler}>
                    <div className="form-group row mt-2 pt-3">
                        <div className="col-sm-2">
                            <label>Fisrt Name</label>
                        </div>
                        <div className="col-sm-6  ">
                            <input onChange={this.onInputChange} className="form-control" placeholder="Your name"></input>
                        </div>
                    </div>
                    <div className="form-group row mt-2">
                        <div className="col-sm-2">
                            <label>Last Name</label>
                        </div>
                        <div className="col-sm-6 ">
                            <input onChange={this.onInputChange} className="form-control" placeholder="Your last name"></input>
                        </div>
                    </div>
                    <div className="form-group row mt-2">
                        <div className="col-sm-2">
                            <label>Country</label>
                        </div>
                        <div className="col-sm-6 ">
                            <select onChange={this.onInputChange} className="form-control" placeholder="...">
                                <option value="aus">Australia</option>
                                <option value="usa">USA</option>
                                <option value="vn">Viet Nam</option>
                            </select>
                        </div>
                    </div>
                    <div className="form-group row mt-2 ">
                        <div className="col-sm-2">
                            <label>Subject </label>
                        </div>
                        <div className="col-sm-6 ">
                            <input onChange={this.onInputChange} className="form-control" placeholder="Write something" style={{height:"5rem"}}></input>
                        </div>
                    </div>
                    <div>
                        <button type="sumbit" className="btn btn-success mt-2" >send data</button>
                    </div>
                    </form>

            </div>
        )
    }
}
export default FormComponent