import "bootstrap/dist/css/bootstrap.min.css"
import TitleComponent from "./components/TitleComponent"
import FormComponent from "./components/FormComponent"
function App() {
  return (
    <div>
      <TitleComponent></TitleComponent>
      <FormComponent></FormComponent>
    </div>
  );
}

export default App;
